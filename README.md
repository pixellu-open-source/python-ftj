# For the Jinja!

## Requirements

- Python 3.5+
- Standalone build: Docker, Python 3.5

## Installing

`pip3 install git+https://bitbucket.org/pixellu-open-source/python-ftj`

### Standalone Build

Standalone builds can reduce the amount of space required when
deploying ftj in the server environment. For example, Linux standalone build
is less than 10MB in size. This includes the Python executable and
all of the dependencies.

The output of a standalone build is in the dist/ directory.

#### Target: Linux

1. Install Docker.
2. Run `./build-standalone.sh`

#### Target: macOS

1. Install development version of pyinstaller: `pip3 install git+https://github.com/pyinstaller/pyinstaller.git`
2. Produce a standlone executable: `pyinstaller --name ftj -F ftj/__init__.py`

## Quick Overview

Use `ftj` to process Jinja templates in a directory tree.
Variables can be defined in a YaML in the root directory (`ftj.yml`).

*Example:*

Directory structure:

```
.
|-- ftj.yml
|-- subdir1
|   `-- template.txt.ftj
`-- hello-world.yml
```

ftj.yml:

```yaml
variable1: world

```

template.txt.ftj:

```
Hello, {{ variable1 }}!
```

Running `ftj` in the root directory will produce a new file in subdir1 --
template.txt, with the following contents:

```
Hello, world!
```

To clean all of the files generated from templates with .ftj extension,
you can run `ftj clean`.

`ftj inspect` displays the defined variables:

```
========================================
Context: /
========================================
variable1: world
```

## TODO

1. Ensure compatibility with Windows.
2. Finish documenting multi-level contexts.

## Advanced Features

### Preprocessing Variable Definitions with Jinja

Context file (`ftj.yml`) is preprocessed with Jinja. The following functions are available (note that these functions
are not accessible from within the actual .ftj templates):

#### `shell(command)`

Runs a shell command and returns the output of the command

*Example:*

```
git_commit: {{ shell("git rev-parse HEAD | cut -b 1-8") }}

```

`git_commit` variable now contains the output of the git command (first 8 characters of the most recent commit hash).

### Specifying Custom Context File Name

CLI argument `--file` (`-f`) is used to specify custom context file.

*Example:*
`ftj -f ftj-vars.yml` will use `ftj-vars.yml` as a context file.

### Adding Prefix/Suffix to Output Files

CLI arguments `--prefix` and `--suffix` are used to add prefix/suffix to the output files.

*Example:*

`ftj --suffix '.production'` will create a file `docker-compose.production.yml` out of the template file `docker-compose.yml.ftj`. Note that the suffix is added before the extension.

Run `ftj --suffix '.production' clean` to remove all the generated files using that suffix.

### Using Root Keys

Instead of loading variables from the root of a context file, FTJ can load variables from a dictionary assigned to a root key in a context file.

*Examples:*

ftj.yml:

```
$prod:
  db:
    host: db-server.example.prod
    user: dbuser
    password: verystrongpassword
  host: www.example.com

$dev:
  db:
    host: db-server.local
    user: dbuser
    password: verystrongpassword
  host: dev.example.local
  port: 8080

$_default:
  port: 80
  db:
    port: 3306
```

If present, the dictionary `$_default` is always loaded. FTJ will then merge values from a specified root key when running `ftj -r <root_key>`.
The merging of values happens *recursively* (see `db.port` example below).

When running `ftj -r prod`, the context will look like this:

```
_root_key: prod
db:
  host: db-server.example.prod
  password: verystrongpassword
  port: 3306
  user: dbuser
host: www.example.com
port: 80
```

While running `ftj -r dev`, the context will be:

```
_root_key: dev
db:
  host: db-server.local
  password: verystrongpassword
  port: 3306
  user: dbuser
host: dev.example.local
port: 8080
```

Note that the variable `_root_key` contains the name of the specified root key. Also, note how the `db.port` has been merged from the `$_default` dictionary.

The '$' prefix in root key names is required if a context file is meant to be used with root keys (as a defensive mechanism).
