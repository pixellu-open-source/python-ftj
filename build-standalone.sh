#!/usr/bin/env bash

# ==============================================================================
# Description
# ==============================================================================
# Starts Ubuntu Docker container to build a single-file Linux distribution
# of FTJ using pyinstaller. Output is stored in dist/ftj.
# ==============================================================================

set -e

docker build -t ftj-build-standalone ./docker/build-standalone/
docker run -it -v "$PWD:/opt/ftj" ftj-build-standalone /bin/bash -c "
pip3 install -e ./
pyinstaller --name ftj -F ftj/__init__.py
"
rm -R ./build/ ./ftj.egg-info/ ./ftj.spec