#!/usr/bin/env python3

import sys
import os
import os.path
import argparse
import fnmatch
import logging
import subprocess
import copy
import re

import yaml
import jinja2


DEFAULT_CONTEXT_FILE_NAME = "ftj.yml"
CONTEXT_ROOT_KEY_VAR_NAME = "_root_key"

TEMPLATE_EXTENSION = "ftj"

ROOT_KEY_DEFAULT = '_default'
ROOT_KEY_PREFIX_CHAR = "$"


EXIT_CODE_GENERIC = 1
EXIT_CODE_CONTEXT_FILE_NOT_FOUND = 101


# ==============================================================================
# Internal Template Functions
# ==============================================================================

def shell(command):
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    output = p.stdout.read()
    p.wait()
    return str(output, 'utf-8').strip()


# ==============================================================================
# FTJ Class
# ==============================================================================

class FTJ(object):

    def __init__(self, root_dir, root_key=None, prefix=None, suffix=None, root_file_name=None):
        self._root_dir = root_dir.rstrip(os.path.sep)
        self._root_key = root_key
        self._ctx_manager = None
        self._template_file_paths = None
        self._output_prefix = prefix
        self._output_suffix = suffix

        self._root_file_name = DEFAULT_CONTEXT_FILE_NAME
        if root_file_name is not None:
            self._root_file_name = root_file_name

    def load(self):
        if self._template_file_paths is None:
            self._template_file_paths = self._find_templates()
        self._ctx_manager = ContextManager(self._root_dir, self._root_key, self._root_file_name)
        self._ctx_manager.load_contexts()

    @property
    def root_context(self):
        return self._ctx_manager.root_context

    @property
    def root_key(self):
        return self._root_key

    @property
    def root_dir(self):
        return self._root_dir

    def _find_templates(self):
        ret = []
        for root, sub_dirs, file_names in os.walk(self._root_dir):
            for file_name in fnmatch.filter(file_names, "*.%s" % TEMPLATE_EXTENSION):
                file_path = os.path.join(root, file_name)
                ret.append(file_path)
                logging.debug("Found file %s", file_path)
        return ret

    def produce_files(self):
        FileProducer.produce_files(self._template_file_paths, self._ctx_manager, self._output_prefix, self._output_suffix)

    def clean(self):
        if self._template_file_paths is None:
            self._template_file_paths = self._find_templates()
        FileProducer.clean_files(self._template_file_paths, self._output_prefix, self._output_suffix)

    def describe_contexts(self):
        values = self._ctx_manager.contexts_values

        ret = ""
        for context_path, values in values.items():
            ret += "========================================\n"
            ret += "Context: " + context_path + "\n"
            ret += "========================================\n"
            ret += yaml.dump(values, default_flow_style=False)
            ret += "\n\n"

        return ret.strip() + "\n"


# ==============================================================================
# ContextManager Class
# ==============================================================================

class ContextManagerError(ValueError):
    """
    Base class for ContextManager errors.
    """
    pass


class ContextManagerContextFileNotFound(ContextManagerError):
    """
    Raised when no context files are found in the root directory.
    """
    pass


class ContextManager(object):

    def __init__(self, root_dir, root_key=None, root_file_name=None):
        self._root_dir = root_dir.rstrip(os.path.sep)
        self._root_key = root_key
        self._contexts = None
        self._root_dir_re = re.compile(r'^%s' % re.escape(root_dir))
        self._max_level = None

        self._root_file_name = DEFAULT_CONTEXT_FILE_NAME
        if root_file_name is not None:
            self._root_file_name = root_file_name

        pass

    def load_contexts(self):
        # Discover contexts:
        contexts = self.discover_contexts()
        if len(contexts) == 0:
            raise ContextManagerContextFileNotFound("No context file present in the root directory.")
        max_level = -1

        # Load the values:
        for level in sorted(contexts.keys()):
            for context_path in contexts[level]:
                absolute_context_path = os.path.join(
                    self._root_dir + context_path,
                    self._root_file_name
                )

                contexts[level][context_path] = Context(
                    absolute_context_path,
                    self._root_key
                )
            max_level = level

        assert max_level >= 0

        # Merge the contexts (b into a):
        for level_a in range(max_level, -1, -1):
            if level_a not in contexts:
                continue
            for level_a_context in contexts[level_a].values():
                for level_b in range(level_a - 1, -1, -1):
                    if level_b not in contexts:
                        continue
                    for level_b_context in contexts[level_b].values():
                        match = re.match(r'^%s' % re.escape(level_b_context.root_dir), level_a_context.root_dir)
                        if match is not None:
                            level_a_context.merge(level_b_context)

        self._contexts = contexts
        self._max_level = max_level

    def context_for_dir(self, dir):
        relative_path = self._root_dir_re.sub("", dir)
        dir_level = self._levels_count_for_relative_path(relative_path)

        # Descend from dir level down until we find a matching context:
        for level in range(dir_level, -1, -1):
            if level not in self._contexts:
                continue
            for context in self._contexts[level].values():
                if re.match(r'^%s' % re.escape(context.root_dir), dir):
                    return context
            break  # No need to descend any further

        return None

    @property
    def root_key(self):
        return self._root_key

    @property
    def root_context(self):
        return list(self._contexts[0].values())[0]

    @property
    def contexts_values(self):
        ret = {}
        for level in self._contexts:
            for context_path in self._contexts[level]:
                ret[context_path] = self._contexts[level][context_path].values

        return ret

    @property
    def contexts(self):
        return self._contexts

    def discover_contexts(self):
        """
        Scans the subdirectories for ftj.yml files.        
        """
        contexts = {}
        for root, sub_dirs, file_names in os.walk(self._root_dir):
            for file_name in fnmatch.filter(file_names, self._root_file_name):
                file_path = os.path.join(root, file_name)
                logging.debug("Found context file %s", file_path)
                relative_path = self._root_dir_re.sub("", root)
                if relative_path == "":
                    relative_path = os.path.sep
                level = self._levels_count_for_relative_path(relative_path)
                # Check if there is a context file in the current ('root') directory:
                if level > 0 and len(contexts) == 0:
                    return contexts
                if level not in contexts:
                    contexts[level] = {}
                contexts[level][relative_path] = {}

        return contexts

    def _levels_count_for_relative_path(self, relative_path):
        relative_path = relative_path.strip()
        if relative_path == os.path.sep:
            return 0
        else:
            return relative_path.count(os.path.sep)


# ==============================================================================
# Context Class
# ==============================================================================

class ContextError(ValueError):
    """
    Base class for context file errors.
    """
    pass


class ContextMixingRootKeysError(ContextError):
    """
    Raised when a context file is mixing root keys and non-root keys.
    """
    pass


class ContextMissingRootKeyArgumentError(ContextError):
    """
    Raised when a context file requires a root key to be specified, but it has not been specified.
    """
    pass


class ContextRootKeyNotFoundError(ContextError):
    """
    Raised when a context file does not contain a specified root key. 
    """
    pass


class Context(object):

    def __init__(self, file_path=None, root_key=None):
        self._raw_values = {}
        self._composed_values = {}
        self._file_path = None
        self._root_dir = None

        self._root_key = root_key
        if file_path is not None:
            self._load_from_file(file_path)

    @property
    def root_dir(self):
        return self._root_dir

    @property
    def root_key(self):
        return self._root_key

    @property
    def values(self):
        return self._composed_values

    def _load_from_dict(self, context_dict):
        self._file_path = None
        self._root_dir = None
        self._raw_values = copy.deepcopy(context_dict)
        self._compose_values()

    def _load_from_file(self, file_path):

        root_dir = os.path.dirname(file_path)

        jinja_env = jinja2.Environment()
        jinja_env.globals['shell'] = shell

        with open(file_path, "r") as fp:
            template_contents = fp.read()

        # We'll be passing the context through Jinja:
        template = jinja_env.from_string(template_contents)

        # Change into the context's dir to process shell commands from within that dir:
        old_cwd = os.getcwd()
        os.chdir(root_dir)

        # Render Jinja template and pass through yaml parser:
        self._raw_values = yaml.load(template.render())

        # Restore previous working directory:
        os.chdir(old_cwd)

        self._file_path = file_path
        self._root_dir = root_dir

        # Compose values
        self._compose_values()

    def _compose_values(self):
        """
        Composes the values according to the root key.
        :return: None
        """
        assert type(self._raw_values) is dict
        assert type(self._root_key) is str or self._root_key is None

        is_root_key_required = self._validate_root_keys()

        if is_root_key_required:
            if self._root_key is not None:
                assert type(self._root_key) is str
                if self._prefix_root_key(self._root_key) not in self._raw_values:
                    raise ContextRootKeyNotFoundError("Root key '%s' not found in the context file '%s'." %
                                                      (self._root_key, self._file_path))
                if self._prefix_root_key(ROOT_KEY_DEFAULT) in self._raw_values:
                    # First, load the default values:
                    assert type(self._raw_values[self._prefix_root_key(ROOT_KEY_DEFAULT)]) is dict
                    self._composed_values = copy.deepcopy(self._raw_values[self._prefix_root_key(ROOT_KEY_DEFAULT)])
                    self._composed_values[CONTEXT_ROOT_KEY_VAR_NAME] = self._root_key
                else:
                    self._composed_values = {}
                assert type(self._raw_values[self._prefix_root_key(self._root_key)]) is dict
                # Now, merge from the root key (recursive merge, with overwriting):
                Context.merge_dict(
                    self._composed_values,  # Merge target
                    copy.deepcopy(self._raw_values[self._prefix_root_key(self._root_key)]),  # Merge source
                    allow_conflicts=True,
                    overwrite_conflicts=True  # Overwrite the values from source into target, if conflicts are present
                )
            else:
                raise ContextMissingRootKeyArgumentError("Context file '%s' requires a root key to be specified." %
                                                         (self._file_path))
        else:
            self._composed_values = copy.deepcopy(self._raw_values)


    @classmethod
    def _prefix_root_key(cls, root_key):
        """
        Adds a root key prefix character to the input.
        :param root_key: root key name to be processed.
        :return: root key as it appears in the configuration file.
        """
        return ROOT_KEY_PREFIX_CHAR + root_key

    def _validate_root_keys(self):
        """
        Checks if there is at least one non-root key among the root keys -- throws an exception if there is.
        Returns True if all of the keys are root keys.
        :param raw_values: raw values of a Context.
        :raises ContextMissingRootKeyException if a mix of keys is detected.
        :return: True if all of the keys are root keys; False otherwise.
        """
        root_keys_count = sum(1 if x.startswith(ROOT_KEY_PREFIX_CHAR) else 0 for x in self._raw_values.keys())
        non_root_keys_count = sum(1 if not x.startswith(ROOT_KEY_PREFIX_CHAR) else 0 for x in self._raw_values.keys())

        if root_keys_count > 0 and non_root_keys_count > 0:
            raise ContextMixingRootKeysError("Cannot mix root keys and non-root keys in a context file.")

        return root_keys_count > 0

    def merge(self, b):
        self.merge_dict(self._composed_values, b.values, allow_conflicts=True)

    @classmethod
    def merge_dict(cls, a, b, path=None, allow_conflicts=False, overwrite_conflicts=False):
        """
        Recursively merges dict b into dict a.
        """
        assert isinstance(a, dict)
        assert isinstance(b, dict)
        if path is None: path = []
        for key in b:
            if key in a:
                if isinstance(a[key], dict) and isinstance(b[key], dict):
                    cls.merge_dict(
                        a[key], b[key],
                        path=path + [str(key)],
                        allow_conflicts=allow_conflicts,
                        overwrite_conflicts=overwrite_conflicts
                    )
                elif a[key] == b[key]:
                    pass  # same leaf value
                else:
                    if allow_conflicts:
                        if overwrite_conflicts:
                            a[key] = b[key]
                    else:
                        raise Exception('Dictionary merge conflict at %s' % '.'.join(path + [str(key)]))
            else:
                a[key] = b[key]
        return a

    def __getitem__(self, item):
        return self._composed_values[item]

# ==============================================================================
# FileProducer Class
# ==============================================================================

class FileProducer(object):

    @classmethod
    def file_path_for_template(cls, template_file_path, prefix=None, suffix=None):
        dir_name, file_name = os.path.split(template_file_path)
        file_name = os.path.splitext(file_name)[0]

        if prefix is not None or suffix is not None:
            file_name, extension = os.path.splitext(file_name)
            if prefix is not None:
                assert type(prefix) is str
                file_name = prefix + file_name
            if suffix is not None:
                assert type(suffix) is str
                file_name = file_name + suffix

            if len(extension) > 0:
                file_name = "%s%s" % (file_name, extension)

        ret = os.path.join(dir_name, file_name)
        logging.debug("File name for template: %s", ret)
        return ret

    @classmethod
    def generate_file_from_template(self, env, template_file_path, output_file_path, context):
        with open(template_file_path, "r") as fp:
            template_contents = fp.read()
        template = env.from_string(template_contents)
        output = template.render(context.values)

        with open(output_file_path, "w", newline='\n') as fp:
            logging.debug("Writing template to %s ...", output_file_path)
            fp.write(output)

    @classmethod
    def produce_files(cls, template_file_paths, context_manager, prefix=None, suffix=None):

        env = jinja2.Environment()

        for template_file_path in template_file_paths:
            dir_name = os.path.dirname(template_file_path)
            context = context_manager.context_for_dir(dir_name)
            output_file_path = FileProducer.file_path_for_template(template_file_path, prefix=prefix, suffix=suffix)
            FileProducer.generate_file_from_template(env, template_file_path, output_file_path, context)

    @classmethod
    def clean_files(cls, template_file_paths, prefix=None, suffix=None):
        count = 0
        for file_name in template_file_paths:
            output_file_name = FileProducer.file_path_for_template(file_name, prefix=prefix, suffix=suffix)
            if os.path.exists(output_file_name):
                logging.info("Removing %s ...", output_file_name)
                os.remove(output_file_name)
                count += 1
        logging.info("Cleaned %d file(s)" % count)


# ==============================================================================
# Main Body
# ==============================================================================

def parse_arguments(cli_args=None):

    parser = argparse.ArgumentParser()

    parser.add_argument('--verbose', '-v', action='store_true',
                        help='Verbose output')
    parser.add_argument('--prefix', type=str,
                        help="Prefix for output files")

    parser.add_argument('--suffix', type=str,
                        help="Suffix for output files")
    parser.add_argument('--root', '-r', dest="root_key", type=str,
                        help="Set a specific root key of the configuration dictionary")
    parser.add_argument('--file', '-f', dest="root_file", type=str,
                        help="Set a specific root key of the configuration dictionary")

    subparsers = parser.add_subparsers(dest='action')
    subparsers.add_parser('clean')
    subparsers.add_parser('inspect')
    get_parser = subparsers.add_parser('get')
    get_parser.add_argument('variable_name', type=str, help="Name of the variable to retrieve")

    return parser.parse_args(cli_args)


def main(cli_args=None):

    args = parse_arguments(cli_args)
    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO, format="%(levelname)s: %(message)s")

    try:

        ftj = FTJ(os.getcwd(), args.root_key, args.prefix, args.suffix, args.root_file)

        if args.action is None:
            ftj.load()
            ftj.produce_files()

        elif args.action == "get":
            ftj.load()
            # TODO: Allow accessing sub-keys
            print(ftj.root_context[args.variable_name], end="")

        elif args.action == "inspect":
            ftj.load()
            print(ftj.describe_contexts())

        elif args.action == "clean":
            ftj.clean()

    except ContextManagerContextFileNotFound as e:
        logging.error(e.args[0])
        sys.exit(EXIT_CODE_CONTEXT_FILE_NOT_FOUND)
    except ContextManagerError as e:
        logging.error(e.args[0])
        sys.exit(EXIT_CODE_GENERIC)
    except ContextError as e:
        logging.error(e.args[0])
        sys.exit(EXIT_CODE_GENERIC)

    sys.exit(0)


if __name__ == '__main__':
    main()