import ftj
import unittest
import os

EXPECTED_CONTENTS_1 = \
"""This is a test.

Test dict key:
Test 1a"""


EXPECTED_CONTENTS_2 = \
"""This is a test number 2.

item1
item2

Test 3!

test123"""


EXPECTED_CONTENTS_3 = \
"""Key 1 = value_1a
Key 2 = value_2c
Root = root_1"""


EXPECTED_CONTENTS_4_ROOT_1 = \
"""key_1 = value_1a
key_2 = value_2c
key_3.subkey_1 = value_3_1a
key_3.subkey_2 = value_3_2a
key_3.subkey_3.subkey_4a = value_3_3_4a
key_3.subkey_3.subkey_4b = value_3_3_4c
key_3.subkey_3.subkey_4c = value_3_3_4c
_root_key = root_1"""

EXPECTED_CONTENTS_4_ROOT_2 = \
"""key_1 = value_1b
key_2 = value_2b
key_3.subkey_1 = value_3_1b
key_3.subkey_2 = value_3_2b
key_3.subkey_3.subkey_4a = 
key_3.subkey_3.subkey_4b = value_3_3_4b
key_3.subkey_3.subkey_4c = value_3_3_4c
_root_key = root_2"""

EXPECTED_CONTENTS_5_FILE_1 = \
"""This is a test.

Test dict key:
Value from ftj-one"""

EXPECTED_CONTENTS_5_FILE_2 = \
"""This is a test.

Test dict key:
Value from ftj-two"""


class FTJUnitTests(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None
        self.init_dir = os.getcwd()

    def tearDown(self):
        os.chdir(self.init_dir)

    def testContextManager(self):

        test_dir = self.init_dir + "/ftj/tests-data/test-multicontext"

        cm = ftj.ContextManager(test_dir)

        expected = {
            0: {
                "/": {
                }
            },
            1: {
                "/subdir2": {
                },
                "/subdir3": {
                }
            },
            2: {
                "/subdir3/subdir4": {
                }
            },
            4: {
                "/subdir6/subdir7/subdir7a/subdir7b": {
                }
            }

        }

        actual = cm.discover_contexts()
        self.assertDictEqual(actual, expected)

        cm.load_contexts()
        expected = {

            "/": {
                "y1k1": "y1v1",
                "y1k2": "y1v2",
                "y1k3": {
                    "y1k31": "y1v31",
                    "y1k32": "y1vk32"
                }
            },

            "/subdir2": {
                "y1k2": "y1v2",
                "y1k3": {
                    "y1k31": "y1v31",
                    "y1k32": "y1vk32"
                },

                "y2k1": "y2v1",
                "y2k2": "y2v2",
                "y2k3": {
                    "y2k31": "y2v31",
                    "y2k32": "y2vk32"
                },
                "y1k1": "y2v1"
            },

            "/subdir3": {
                "y1k1": "y1v1",
                "y1k2": "y1v2",
                "y3k1": "y3v1",
                "y3k2": "y3v2",
                "y1k3": {
                    "y1k31": "y1v31",
                    "y1k32": "y1vk32",
                    "y3k31": "y2v31",
                    "y3k32": "y2vk32"
                }
            },

            "/subdir3/subdir4": {
                "y1k1": "y1v1",
                "y1k2": "y1v2",

                "y3k1": "y3v1",
                "y3k2": "y3v2",
                "y1k3": {

                    "y1k31": "y1v31",
                    "y1k32": "y1vk32",

                    "y3k31": "y4v31",
                    "y3k32": "y4v32",
                    "y4k33": "y4v33",
                },
                "y4k1": "ftj.yml test4.conf"  # TODO: Fix the test for Windows platform
            },

            "/subdir6/subdir7/subdir7a/subdir7b": {
                "y1k1": "y1v1",
                "y1k3": {
                    "y1k31": "y1v31",
                    "y1k32": "y1vk32"
                },
                "y1k2": "y7v2",
                "y7k4": "y7v4"

            }
        }

        actual = cm.contexts_values
        self.assertDictEqual(actual, expected)

        ctx = cm.context_for_dir(os.path.join(test_dir))
        self.assertEqual(ctx['y1k1'], 'y1v1')

        ctx = cm.context_for_dir(os.path.join(test_dir, "subdir2"))
        self.assertEqual(ctx['y2k1'], 'y2v1')

        ctx = cm.context_for_dir(os.path.join(test_dir, "subdir3"))
        self.assertEqual(ctx['y3k2'], 'y3v2')
        self.assertEqual(ctx['y1k1'], 'y1v1')

        ctx = cm.context_for_dir(os.path.join(test_dir, "subdir3", "subdir4"))
        self.assertEqual(ctx['y3k2'], 'y3v2')

        ctx = cm.context_for_dir(os.path.join(test_dir, "subdir6", "subdir7", "subdir7a", "subdir7b"))
        self.assertEqual(ctx['y7k4'], 'y7v4')

    def testContextManagerErrors(self):

        test_dir = self.init_dir + "/ftj/tests-data/test-rootkey/test-3"
        with self.assertRaises(ftj.ContextManagerContextFileNotFound):
            cm = ftj.ContextManager(test_dir)
            cm.discover_contexts()
            cm.load_contexts()


class FTJIntegrationTests(unittest.TestCase):

    def setUp(self):
        self.init_dir = os.getcwd()

    def tearDown(self):
        os.chdir(self.init_dir)

    def assertFileContentsEqual(self, file_name, expected_contents):
        with open(file_name, "r") as fp:
            contents = fp.read()

        if contents != expected_contents:
            raise AssertionError(
                "Contents of file '%s' do not match the expected result:\n%s\n----\n!=\n----\n%s"
                %
                (file_name, contents, expected_contents)
            )

    def testSubdirs(self):
        test_dir = self.init_dir + "/ftj/tests-data/test-subdirs"
        ftj_inst = ftj.FTJ(test_dir)
        ftj_inst.load()
        ftj_inst.produce_files()

        self.assertFileContentsEqual(test_dir + "/test1.conf", EXPECTED_CONTENTS_1)
        self.assertFileContentsEqual(test_dir + "/subdir1/test2.conf", EXPECTED_CONTENTS_2)

        ftj_inst.clean()

    def testRootKey(self):
        test_dir = self.init_dir + "/ftj/tests-data/test-rootkey/test-1"
        ftj_inst = ftj.FTJ(test_dir, "root_1")
        ftj_inst.load()
        ftj_inst.produce_files()
        self.assertFileContentsEqual(test_dir + "/test1.conf", EXPECTED_CONTENTS_3)

        ftj_inst.clean()

    def testRootFile(self):
        test_dir = self.init_dir + "/ftj/tests-data/test-rootfile"
        ftj_inst = ftj.FTJ(test_dir, None, None, None, "ftj-one.yml")
        ftj_inst.load()
        ftj_inst.produce_files()
        self.assertFileContentsEqual(test_dir + "/test.conf", EXPECTED_CONTENTS_5_FILE_1)
        ftj_inst.clean()

        test_dir = self.init_dir + "/ftj/tests-data/test-rootfile"
        ftj_inst = ftj.FTJ(test_dir, None, None, None, "ftj-two.yml")
        ftj_inst.load()
        ftj_inst.produce_files()
        self.assertFileContentsEqual(test_dir + "/test.conf", EXPECTED_CONTENTS_5_FILE_2)
        ftj_inst.clean()

    def testRootKeyMerge(self):
        test_dir = self.init_dir + "/ftj/tests-data/test-rootkey/test-4"
        ftj_inst = ftj.FTJ(test_dir, "root_1")
        ftj_inst.load()
        ftj_inst.produce_files()
        self.assertFileContentsEqual(test_dir + "/test4.conf", EXPECTED_CONTENTS_4_ROOT_1)
        ftj_inst.clean()

        ftj_inst = ftj.FTJ(test_dir, "root_2")
        ftj_inst.load()
        ftj_inst.produce_files()
        self.assertFileContentsEqual(test_dir + "/test4.conf", EXPECTED_CONTENTS_4_ROOT_2)

        ftj_inst.clean()


    def testRootKeyErrors(self):
        test_dir = self.init_dir + "/ftj/tests-data/test-rootkey/test-2"
        with self.assertRaises(ftj.ContextMixingRootKeysError):
            ftj_inst = ftj.FTJ(test_dir)
            ftj_inst.load()

        test_dir = self.init_dir + "/ftj/tests-data/test-rootkey/test-1"
        with self.assertRaises(ftj.ContextRootKeyNotFoundError):
            ftj_inst = ftj.FTJ(test_dir, "root_1a")
            ftj_inst.load()

        test_dir = self.init_dir + "/ftj/tests-data/test-rootkey/test-1"
        with self.assertRaises(ftj.ContextMissingRootKeyArgumentError):
            ftj_inst = ftj.FTJ(test_dir)
            ftj_inst.load()

