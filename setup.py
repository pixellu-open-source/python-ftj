from distutils.core import setup

setup(
    name='ftj',
    version='0.3.0a',
    packages=['ftj'],
    url='',
    license='BSD',
    author='Alex Prykhodko',
    author_email='alex@prykhodko.net',
    description='ftj (For the Jinja!) descends through a directory tree and processes Jinja templates '
                'using variables defined in a YaML file.',
    entry_points={
        'console_scripts': [
            'ftj = ftj:main'
        ]
    },
    install_requires=[
        "Jinja2>=2.9.5",
        "PyYAML>=3.12"
    ]
)
